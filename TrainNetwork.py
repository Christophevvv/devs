#!/usr/bin/env python2
from pypdevs.DEVS import *
from Generator import Generator
from Segment import Segment
from Collector import Collector

class TrainNetwork(CoupledDEVS):
    def __init__(self,length,nr_segments):
        CoupledDEVS.__init__(self,"trainsystem")
        self.generator = self.addSubModel(Generator())
        # self.segment = self.addSubModel(Segment(length,0))
        # self.connectPorts(self.generator.Q_send,self.segment.Q_recv)
        # self.connectPorts(self.segment.Q_sack,self.generator.Q_rack)
        # self.connectPorts(self.generator.train_out,self.segment.train_in)

        self.segments = []
        for i in range(nr_segments):
            self.segments.append(self.addSubModel(Segment(length*1.0/nr_segments,i)))
        self.collector = self.addSubModel(Collector())
        self.connectPorts(self.generator.Q_send,self.segments[0].Q_recv)
        self.connectPorts(self.segments[0].Q_sack,self.generator.Q_rack)
        self.connectPorts(self.generator.train_out,self.segments[0].train_in)
        for i in range(nr_segments-1):
            self.connectPorts(self.segments[i].Q_send,self.segments[i+1].Q_recv)
            self.connectPorts(self.segments[i+1].Q_sack,self.segments[i].Q_rack)
            self.connectPorts(self.segments[i].train_out,self.segments[i+1].train_in)
        self.connectPorts(self.segments[nr_segments-1].Q_send,self.collector.Q_recv)
        self.connectPorts(self.collector.Q_sack,self.segments[nr_segments-1].Q_rack)
        self.connectPorts(self.segments[nr_segments-1].train_out,self.collector.train_in)
