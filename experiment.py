#!/usr/bin/env python2
from TrainNetwork import TrainNetwork
from pypdevs.simulator import Simulator
import random
import matplotlib.pyplot as plt



# random.seed(1)
# t = TrainNetwork(10000,5)
# sim = Simulator(t)
# sim.setVerbose()
# sim.setTerminationTime(36000) #?
# sim.setClassicDEVS()
# sim.simulate()
# print t.collector.state.avg


results = []
results_performance = []

for i in range(1,20):
    random.seed(100)
    t = TrainNetwork(10000,i)
    sim = Simulator(t)
    #sim.setVerbose()
    sim.setTerminationTime(3*3600) #?
    sim.setClassicDEVS()
    sim.simulate()
    results.append(10*i + t.collector.state.avg)
    results_performance.append(t.collector.state.performance)


plt.plot(range(1,20),results)
plt.ylabel('cost')
plt.xlabel('number of segments')
plt.show()

plt.plot(range(1,20),results_performance)
plt.ylabel('IAT collector')
plt.xlabel('number of segments')
plt.show()
