class Train:
    def __init__(self,ID,a_max,departure_time):
        self.ID = ID
        self.a_max = a_max
        self.departure_time = departure_time
        #extra attributes not set at instantiation:
        self.v = 0 #velocity
        self.remaining_x = -1

    def __str__(self):
        return "train_"+str(self.ID)

class Query:
    def __init__(self):
        pass

    def __str__(self):
        return "Query"

class QueryAck:
    def __init__(self,state): #state of light {red,green}
        self.state = state

    def __str__(self):
        return "Answer: "+str(self.state)
