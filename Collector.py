from pypdevs.DEVS import *
from eventEntities import QueryAck

class CollectorState:
    def __init__(self):
        self.pending_query = False
        self.counter = 0
        self.avg = 0.0
        self.sim_time = 0.0

        self.lastArrival = 0.0
        self.performance = 0.0

    def __str__(self):
        if self.pending_query:
            return "Answering Query"
        else:
            return "Idle"

class Collector(AtomicDEVS):
    def __init__(self):
        AtomicDEVS.__init__(self,"Collector")
        self.Q_recv = self.addInPort("Q_recv")
        self.train_in = self.addInPort("train_in")
        self.Q_sack = self.addOutPort("Q_sack")
        self.state = CollectorState()

    def intTransition(self):
        if self.state.pending_query:
            self.state.pending_query = False
        return self.state

    def extTransition(self, inputs):
        self.state.sim_time += self.elapsed

        if inputs.get(self.Q_recv):
            self.state.pending_query = True

        if inputs.get(self.train_in):
            train = inputs.get(self.train_in)

            if self.state.lastArrival != 0.0:
                time_between = self.state.sim_time - self.state.lastArrival
                perf = self.state.performance * (float(self.state.counter) - 1.0)
                perf += time_between
                self.state.performance = perf / float(self.state.counter)
            self.state.lastArrival = self.state.sim_time

            avg = self.state.avg * float(self.state.counter)
            passed_time = self.state.sim_time - train.departure_time
            avg += passed_time
            self.state.counter += 1
            self.state.avg = avg/float(self.state.counter)

        return self.state

    def outputFnc(self):
        output = {}
        # answer query from previous segment:
        if self.state.pending_query:
            output[self.Q_sack] = QueryAck("green")
        return output

    def timeAdvance(self):
        if self.state.pending_query:
            return 0
        else:
            return float('inf')
