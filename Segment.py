from pypdevs.DEVS import *
from eventEntities import *
from formulas import acceleration_formula,brake_formula

class SegmentState:
    def __init__(self,length):
        #length of this segment
        self.length = length
        #Indicate whether there is a train on this segment: {train_object,None}
        self.train = None
        #indicate whether a query request came in
        self.pending_query = False
        #Indicate we are polling for a green light (we are at most 1km from light)
        self.polling = False
        #Indicate we saw a red light and we have to brake
        self.braking = False
        #timer untill next internal transition
        self.remaining = 0
        #time between consecutive polls
        self.t_poll = 1

    def __str__(self):
        result = ""
        if self.polling:
            result += "polling"
        elif self.pending_query:
            result += "pending_query"
        else:
            result += "idle"
        return result
        
class Segment(AtomicDEVS):
    def __init__(self,length,ID):
        AtomicDEVS.__init__(self,"Segment"+str(ID))
        #inputs
        self.Q_recv = self.addInPort("Q_recv")
        self.train_in = self.addInPort("train_in")
        self.Q_rack = self.addInPort("Q_rack")
        #outputs
        self.Q_sack = self.addOutPort("Q_sack")
        self.Q_send = self.addOutPort("Q_send")
        self.train_out = self.addOutPort("train_out")
        #state
        self.state = SegmentState(length)

    def intTransition(self):
        #We sent an answer in output function so we set pending query back to false
        self.state.remaining -= self.timeAdvance()
        if self.state.pending_query:
            self.state.pending_query = False                
            
        if self.state.train == None:
            return self.state
        elif self.state.train.remaining_x == 0:
            self.state.train = None
            return self.state
            
        if self.state.remaining == 0:
            if self.state.polling:
                if self.state.braking:
                    bf = brake_formula(self.state.train.v,self.state.t_poll,self.state.train.remaining_x)
                    self.state.train.v = bf[0]
                    self.state.train.remaining_x -= bf[1]
                else:
                    #We maintain a constant speed when observing the traffic light
                    self.state.train.remaining_x -= self.state.train.v * self.state.t_poll
                assert(self.state.train.remaining_x >= 0) #assert that we did not pass a red light
                self.state.remaining = self.state.t_poll
            else:
                max_v = 120/3.6 # 120 km/h => 120/3.6 m/s                
                af = acceleration_formula(self.state.train.v,max_v,self.state.train.remaining_x,self.state.train.a_max)
                self.state.train.v = af[0]
                self.state.train.remaining_x = 0
                self.state.remaining = af[1]
                #reset the state of braking
                self.state.braking = False
            
        return self.state

    def extTransition(self, inputs):
        self.state.remaining -= self.elapsed
        if inputs.get(self.Q_recv):
            self.state.pending_query = True
        if inputs.get(self.train_in):
            #Note: we assume that a train only comes in when the light is green (no check needed)
            assert(self.state.train == None) #for debugging purpose
            self.state.train = inputs.get(self.train_in)
            #set remaining time to time untill we see traffic light of next segment
            x_remaining = self.state.length - 1000
            if x_remaining < 0:
                self.state.train.remaining_x = self.state.length
                x_remaining = 0
            else:
                self.state.train.remaining_x = 1000
            #set maximum speed of train
            max_v = 120/3.6 # 120 km/h => 120/3.6 m/s
            af = acceleration_formula(self.state.train.v,max_v,x_remaining,self.state.train.a_max)
            self.state.train.v = af[0] #new speed
            self.state.remaining = af[1] #time untill we reach the 1000 m from traffic light point
            #indicate we are now in the polling state
            self.state.polling = True
        if inputs.get(self.Q_rack):
            if inputs.get(self.Q_rack).state == "green":
                self.state.polling = False
            else: #red
                self.state.braking = True
        return self.state

    def outputFnc(self):
        output = {}
        #answer query from previous segment:
        if self.state.pending_query:
            if self.state.train == None:
                output[self.Q_sack] = QueryAck("green")
            else:
                output[self.Q_sack] = QueryAck("red")
        #send query to next segment:
        if self.state.remaining - self.timeAdvance() == 0 and self.state.polling:
            output[self.Q_send] = Query()
        #send train to next segment:
        if self.state.train != None:
            if self.state.train.remaining_x == 0:
                output[self.train_out] = self.state.train
        return output

    def timeAdvance(self):
        if self.state.pending_query:
            #immediate response requirement
            return 0
        #No train means there is nothing to do except for answering queries from previous segment
        elif self.state.train == None:
            return float('inf')
        else:
            #return time untill next internal transition
            return self.state.remaining 
        
