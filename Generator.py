from pypdevs.DEVS import *
from eventEntities import *
import random

class GeneratorState:
    def __init__(self):
        #Timers:
        #Current simulation time (statistics):
        self.current_time = 0.0
        #Remaining time untill next event(train) generation:
        self.train_remaining = 0.0
        #Remaining time untill we poll the first segment:
        self.poll_remaining = 0.0
        #Indicate whether we are polling the first segment:
        self.is_polling = False
        #Indicate whether we received a QueryAck with green state
        self.received_green_ack = False
        #Generator variables:
        #Interarrival times(seconds):
        self.ITA_min = 300
        self.ITA_max = 1500
        #Train acceleration:
        self.a_min = 0.10
        self.a_max = 0.25
        #Train queue
        self.train_queue = []
        #train ID counter
        self.ID_ctr = 0
    def __str__(self):
        if self.train_queue:
            return str(len(self.train_queue)) + " trains in the queue"
        else:
            return "Generating trains"
class Generator(AtomicDEVS):
    def __init__(self):
        AtomicDEVS.__init__(self,"Generator")
        self.train_out = self.addOutPort("train_out")
        self.Q_send = self.addOutPort("Q_send")
        self.Q_rack = self.addInPort("Q_rack")
        self.state = GeneratorState()

    def intTransition(self):
        #Update simulation time:
        advance = self.timeAdvance()
        self.state.current_time += advance
        #Update remaining times:
        if self.state.is_polling:
            self.state.poll_remaining -= advance
        self.state.train_remaining -= advance
        if self.state.received_green_ack: #queue not empty is implicit
            self.state.received_green_ack = False
            #dequeue first train in queue (this one was sent by output function)
            self.state.train_queue.pop(0)
        if self.state.train_remaining == 0.0:
            #New train arrival:
            #Sample a acceleration from uniform distribution
            a = random.uniform(self.state.a_min,self.state.a_max)
            #Add new train to back of the queue
            self.state.train_queue.append(Train(self.state.ID_ctr,a,self.state.current_time))
            #Increase ID counter
            self.state.ID_ctr += 1
            #Schedule next train arrival
            self.state.train_remaining = random.uniform(self.state.ITA_min,self.state.ITA_max)
        if self.state.is_polling and self.state.poll_remaining == 0.0:
            #reset poll timer
            self.state.poll_remaining = 1.0
            
        return self.state
        
    def extTransition(self,inputs):
        #inp is a QueryAck object with state either {red,green}
        inp = inputs[self.Q_rack]
        if inp.state == "green":
            self.state.received_green_ack = True
            #If there is only one train in the queue we no longer have to poll after this.
            if len(self.state.train_queue) == 1:
                self.state.is_polling = False
        else: #state == "red"
            self.state.is_polling = True
            self.state.poll_remaining = 1.0                
        return self.state
                    
    def timeAdvance(self):
        if self.state.received_green_ack:
            return 0
        elif self.state.is_polling:
            return min(self.state.train_remaining,self.state.poll_remaining)
        else:
            return self.state.train_remaining

    def outputFnc(self):
        output = {}
        #Output train if a green ack has been received:
        if self.state.received_green_ack: # and len(self.state.train_queue) > 0: => implicit
            output[self.train_out] = self.state.train_queue[0]
        #Output a query if we are not already polling, and a new train in going to arrive
        if (not self.state.is_polling and self.state.train_remaining - self.timeAdvance() == 0.0) or (self.state.is_polling and self.state.poll_remaining - self.timeAdvance() == 0.0):
            output[self.Q_send] = Query()
        return output
